#/bin/python3
from triangle_checker import check_triangle
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import test_runner


def test_simple() -> bool:
    return "scalene" == check_triangle(3, 4, 5)


def test_equilateral() -> bool:
    return "equilateral" == check_triangle(3, 3, 3)


def test_isosceles() -> bool:
    result = True
    result &= ("isosceles" == check_triangle(1, 3, 3))
    result &= ("isosceles" == check_triangle(2, 3, 3))
    return result


def test_right_angled() -> bool:
    return True


def test_rightangled() -> bool:
    # implement test
    pass


if __name__=='__main__':
    tr = test_runner.TestRunner()
    # add new test
    tr.add_test(test_simple)
    tr.add_test(test_equilateral)
    tr.add_test(test_isosceles)
    # run all test:
    result = tr.run_tests()
