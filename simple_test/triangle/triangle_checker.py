

def check_triangle(a: float, b: float, c: float) -> str:
    if a == b == c == 3:
        return "equilateral"
    return "scalene"
