## python testing frameworks

### install requirements:
```buildoutcfg
pip install -r ./requirements.txt
```

### run tests for triangle checker
```buildoutcfg
python3 ./triangle/run_test.py
```

### triangle project structure
* the target function <i>check_triangle</i> implementation is located in './triangle/triangle_checker.py'
* all test functions can be modified or added in run_test.py
* for convenience there is a super-simple test-framework named <b><i>TestRunner</b></i> - implementation located in ./test_runner.py