

class TestRunner:
    def __init__(self):
        self.test_list_ = []


    def add_test(self, test_function):
        self.test_list_.append(test_function)

    def run_tests(self) -> int:
        incr = 0
        error_count = 0
        for test_name in self.test_list_:
            incr += 1
            try:
                assert test_name()
            except Exception as err:
                error_count += 1
                print(f"Test # {incr} failed: {test_name.__name__}() != true")
        if error_count == 0:
            print(f"All {incr} tests passed!")